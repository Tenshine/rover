
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RoverTest {

    @Test
    void move_north() {
        int start_x = 1;
        int start_y = 1;
        Rover rover = new Rover(start_x, start_y, 5, 5, Direction.NORTH);

        rover.move();

        Assertions.assertEquals(start_x, rover.getPos_x());
        Assertions.assertEquals(start_y + 1, rover.getPos_y());
    }

    @Test
    void move_south() {
        int start_x = 1;
        int start_y = 1;
        Rover rover = new Rover(start_x, start_y, 5, 5, Direction.SOUTH);

        rover.move();

        Assertions.assertEquals(start_x, rover.getPos_x());
        Assertions.assertEquals(start_y - 1, rover.getPos_y());
    }

    @Test
    void move_est() {
        int start_x = 1;
        int start_y = 1;
        Rover rover = new Rover(start_x, start_y, 5, 5, Direction.EST);

        rover.move();

        Assertions.assertEquals(start_x + 1, rover.getPos_x());
        Assertions.assertEquals(start_y, rover.getPos_y());
    }

    @Test
    void move_west() {
        int start_x = 1;
        int start_y = 1;
        Rover rover = new Rover(start_x, start_y, 5, 5, Direction.WEST);

        rover.move();

        Assertions.assertEquals(start_x - 1, rover.getPos_x());
        Assertions.assertEquals(start_y, rover.getPos_y());
    }

    @Test
    void rotate_right() {
        Rover rover = new Rover(0, 0, 5, 5, Direction.EST);

        rover.rotate_right();

        Assertions.assertEquals(Direction.SOUTH, rover.getDirection());
    }

    @Test
    void rotate_rightAtLast() {
        Rover rover = new Rover(0, 0, 5, 5, Direction.WEST);

        rover.rotate_right();

        Assertions.assertEquals(Direction.NORTH, rover.getDirection());
    }

    @Test
    void rotate_leftAtFirst() {
        Rover rover = new Rover(0, 0, 5, 5, Direction.NORTH);

        rover.rotate_left();

        Assertions.assertEquals(Direction.WEST, rover.getDirection());
    }

    @Test
    void moveOutOfMapNorth() {
        int start_x = 5;
        int start_y = 5;
        Rover rover = new Rover(start_x, start_y, 5, 5, Direction.NORTH);

        rover.move();

        Assertions.assertEquals(start_x, rover.getPos_x());
        Assertions.assertEquals(start_y, rover.getPos_y());
    }

    @Test
    void moveOutOfMapSouth() {
        int start_x = 0;
        int start_y = 0;
        Rover rover = new Rover(start_x, start_y, 5, 5, Direction.SOUTH);

        rover.move();

        Assertions.assertEquals(start_x, rover.getPos_x());
        Assertions.assertEquals(start_y, rover.getPos_y());
    }

    @Test
    void moveOutOfMapEst() {
        int start_x = 5;
        int start_y = 5;
        Rover rover = new Rover(start_x, start_y, 5, 5, Direction.EST);

        rover.move();

        Assertions.assertEquals(start_x, rover.getPos_x());
        Assertions.assertEquals(start_y, rover.getPos_y());
    }

    @Test
    void moveOutOfMapWest() {
        int start_x = 0;
        int start_y = 0;
        Rover rover = new Rover(start_x, start_y, 5, 5, Direction.WEST);

        rover.move();

        Assertions.assertEquals(start_x, rover.getPos_x());
        Assertions.assertEquals(start_y, rover.getPos_y());
    }
}