import java.util.ArrayList;
import java.util.List;

public class CommandListFactory {
    public static List<Command> create(Input_information input) {
        List<Command> commands = new ArrayList<>();

        for (Character cmd : input.getCommands().toCharArray()) {
            commands.add(Command.from_char(cmd));
        }

        return commands;
    }
}
