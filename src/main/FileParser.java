import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileParser {

    public static List<Input_information> Parse(String fileName) throws Exception {
        List<Input_information> rovers_information = new ArrayList<>();
        Path path = Paths.get(fileName);
        Scanner scanner = new Scanner(path);

        try {
            Input_information.map_x = scanner.nextInt();
            Input_information.map_y = scanner.nextInt();

            while (scanner.hasNextLine()) {
                int pos_x = scanner.nextInt();
                int pos_y = scanner.nextInt();
                Character direction = scanner.next().charAt(0);
                String commands = scanner.next();
                if (pos_x < 0 || pos_x > Input_information.map_x || pos_y < 0 || pos_y > Input_information.map_y) {
                    throw new Exception("Out of bounds Starting position");
                }
                if (!Direction.exists(direction)) {
                    throw new Exception("Invalid starting direction");
                }
                for (Character command : commands.toCharArray()) {
                    if (!Command.exists(command)) {
                        throw new Exception("Invalid Command");
                    }
                }
                rovers_information.add(new Input_information(pos_x, pos_y, direction, commands));
            }

        } catch (Exception e) {
            throw e;
        }

        return rovers_information;
    }

}
