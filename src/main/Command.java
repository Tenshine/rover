public enum Command {
    RIGHT_ROTATION('R'),
    LEFT_ROTATION('L'),
    MOVE('M');

    private final Character shortCode;

    Command(Character code) {
        shortCode = code;
    };

    public Character getShortCode() {
        return this.shortCode;
    };

    public static Command from_char(Character c) {
        for (Command b : Command.values()) {
            if (b.shortCode.equals(c)) {
                return b;
            }
        }
        return null;
    }

    public static boolean exists(Character c) {
        for (Command b : Command.values()) {
            if (b.shortCode.equals(c)) {
                return true;
            }
        }
        return false;
    }
}
