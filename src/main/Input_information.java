import java.util.ArrayList;
import java.util.List;

public class Input_information {
    public static int map_x, map_y;
    private int pos_x, pos_y;
    private char direction;
    private String commands;

    public Input_information(int x, int y, char direction, String commands) {
        this.pos_x = x;
        this.pos_y = y;
        this.direction = direction;
        this.commands = commands;
    }

    public int getPos_x() {
        return pos_x;
    }

    public int getPos_y() {
        return pos_y;
    }

    public char getDirection() {
        return direction;
    }

    public String getCommands() {
        return commands;
    }
}
