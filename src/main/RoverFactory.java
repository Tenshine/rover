public class RoverFactory {
    public static Rover create(Input_information input) {
        return new Rover(
                input.getPos_x(),
                input.getPos_y(),
                input.map_x,
                input.map_y,
                Direction.from_char(input.getDirection())
        );
    }
}
