import java.util.*;

public class Plateau {

    private Map<Rover, List<Command>> rovers_commands = new LinkedHashMap<>();

    public void addRover_commands(Rover rover, List<Command> rover_commands) {
        this.rovers_commands.put(rover, rover_commands);
    }

    public Map<Rover, List<Command>> getRovers_commands() {
        return rovers_commands;
    }

    public Set<Rover> getRovers() {
        return rovers_commands.keySet();
    }

    public Collection<List<Command>> getCommands() {
        return rovers_commands.values();
    }

    public void printRoversPosition() {
        for (Rover rover : getRovers()) {
            rover.print_position();
        }
    }
}
