import java.util.List;

public class PlateauFactory {

    public static Plateau create(List<Input_information> inputs) {
        Plateau plateau = new Plateau();

        for (Input_information input : inputs) {
            Rover rover = RoverFactory.create(input);
            List<Command> commands = CommandListFactory.create(input);

            plateau.addRover_commands(rover, commands);
        }

        return plateau;
    }
}
