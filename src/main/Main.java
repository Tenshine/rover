import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length > 0) {
            try {
                List<Input_information> parsed_input = FileParser.Parse(args[0]);
                Plateau plateau = PlateauFactory.create(parsed_input);

                Controller.exec_commands(plateau);
                plateau.printRoversPosition();

            } catch (Exception e) {
                System.out.println(e);
                throw e;
            }
        }
    }
}
