import java.util.List;
import java.util.Map;

public class Controller {
    private static void exec_command(Rover rover, List<Command> commands) {
        for (Command cmd : commands) {
            switch (cmd) {
                case MOVE:
                    rover.move();
                    break;
                case LEFT_ROTATION:
                    rover.rotate_left();
                    break;
                case RIGHT_ROTATION:
                    rover.rotate_right();
                    break;
            }
        }
    }

    public static void exec_commands(Plateau plateau) {
        for (Map.Entry<Rover, List<Command>> rover_command : plateau.getRovers_commands().entrySet()) {
            exec_command(rover_command.getKey(), rover_command.getValue());
        }
    }
}
