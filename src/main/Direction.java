
public enum Direction {
    NORTH('N') {
        @Override
        public Direction prev() {
            return values()[values().length - 1];
        }
    },
    EST('E'),
    SOUTH('S'),
    WEST('W') {
        @Override
        public Direction next() {
            return values()[0];
        }
    };

    private final Character shortCode;

    Direction(Character code) {
        shortCode = code;
    };

    public Character getShortCode() {
        return this.shortCode;
    };

    public static Direction from_char(Character c) {
        for (Direction b : Direction.values()) {
            if (b.shortCode.equals(c)) {
                return b;
            }
        }
        return null;
    }

    public static boolean exists(Character c) {
        for (Direction b : Direction.values()) {
            if (b.shortCode.equals(c)) {
                return true;
            }
        }
        return false;
    }

    public Direction next() {
        return values()[ordinal() + 1];
    };

    public Direction prev() {
        return values()[ordinal() - 1];
    };
};