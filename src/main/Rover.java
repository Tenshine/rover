public class Rover {

    private int pos_x, pos_y;
    private final int max_x, max_y, min_x, min_y;
    private Direction direction;

    public Rover(int start_x, int start_y, int max_x, int max_y, Direction direction) {
        this.pos_x = start_x;
        this.pos_y = start_y;
        this.direction = direction;

        this.max_x = max_x;
        this.max_y = max_y;
        this.min_x = 0;
        this.min_y = 0;
    }

    public void move() {
        switch (direction) {
            case NORTH:
                if (pos_y < max_y)
                    ++pos_y;
                break;
            case SOUTH:
                if (pos_y > min_y)
                    --pos_y;
                break;
            case EST:
                if (pos_x < max_x)
                    ++pos_x;
                break;
            case WEST:
                if (pos_x > min_x)
                    --pos_x;
                break;
        }
    }

    public int getPos_y() {
        return pos_y;
    }

    public int getPos_x() {
        return pos_x;
    }

    public Direction getDirection() {
        return direction;
    }

    public void rotate_right() {
        direction = direction.next();
    }

    public void rotate_left() {
        direction = direction.prev();
    }

    public void print_position() {
        System.out.format("%d %d %c\n", pos_x, pos_y, direction.getShortCode());
    }
}
